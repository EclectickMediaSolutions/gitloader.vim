" Attempt to make a simpler git branch mkview loader

" Path stuffs
let s:working_dir = getcwd()
let s:session_dir = '.vimsessions'
let s:views_dir = 'views'
let s:dir = join([s:working_dir, s:session_dir], '/')
" let s:view_dir = join([s:dir, s:views_dir], '/')

" HELPER FUNCS
function! s:replace_bad_chars(string)
  return substitute(a:string, '/', '_', 'g')
endfunction

function! s:trim(string)
  return substitute(substitute(a:string, '^\s*\(.\{-}\)\s*$', '\1', ''), '\n', '', '')
endfunction

function! s:git_branch_name()
  return s:replace_bad_chars(s:trim(system("\git rev-parse --abbrev-ref HEAD")))
endfunction

function! s:in_git_repo()
  let l:is_git_repo = system("\git rev-parse --git-dir >/dev/null")
  return v:shell_error == 0
endfunction

" LOGICAL FUNCS
function! s:create_folder(path)
  if !isdirectory(a:path)
    call mkdir(a:path, 'p')
    echo 'gitloader.vim made path: ' . a:path
  endif
  return 0
endfunction

function! s:create_git_folder()
  if !s:in_git_repo()
    echoerr "FATAL! NOT GIT REPO"
    return 1
  endif

  call s:create_folder(s:dir)
  let a:branch_dir = join([s:dir, s:git_branch_name()], '/')
  call s:create_folder(a:branch_dir)
  let a:views_folder = join([a:branch_dir, s:views_dir], '/')
  call s:create_folder(a:views_folder)

  return [a:branch_dir, a:views_folder]
endfunction

function s:create_base_folder()
  call s:create_folder(s:dir)
  let a:views_folder = join([s:dir, s:views_dir], '/')
  call s:create_folder(a:views_folder)

  return [a:views_folder]
endfunction

if s:in_git_repo()
  let s:paths = s:create_git_folder()
  let s:file = join([s:paths[-1], expand('%:t')], '/')
else
  let s:paths = s:create_base_folder()
  let s:file = join([s:paths[-1], expand('%:t')], '/')
endif

function! s:Save()
  execute 'mkview!' s:file
  echom "Saved:" s:file
endfunction

function! s:Load()
  execute 'source' s:file
  redraw!
  echom "Loaded:" s:file
endfunction

" COMMAND DEFINITION
command! Save call s:Save()
command! Load call s:Load()
command! InGitRepo call s:in_git_repo()

augroup gitloader
  au! VimLeave ?* Save
  au! VimEnter ?* silent Load
augroup END

" vim: set nospell:
" vim: set viewoptions-=options:
